const express = require('express');
const res = require('express/lib/response');
const router = express.Router();

const {
	createTask,
	getAllTasks,
	deleteTask,
	updateTask,
	getTask

} = require('./../controllers/taskControllers')

//CREATE A TASK
router.post('/', async (req, res) => {
	// console.log(req.body)	{"name": "watching"}	//object

	try{
		await createTask(req.body).then(result => res.send(result))

	} catch(err){
		res.status(400).json(err.message)

	} 
})
 


//GET ALL TASKS
router.get('/', async (req, res) => {
	try{
		await getAllTasks().then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})



//DELETE A TASK

router.delete('/:id/delete', async (req, res) => {
	// console.log(typeof req.params)	//object
	//console.log(typeof req.params.id)	//string

	try{
		await deleteTask(req.params.id).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})



//UPDATE A TASK
router.put('/', async (req, res) => {
	// console.log(req.params.taskId)
	const id = req.params.taskId
	// console.log(req.body)

	//await updateTask(id, req.body).then(response => res.send(response))
	//using findOne, we can remove the /:taskId
	await updateTask(id, req.body.name, req.body).then(response => res.send(response))
})


module.exports = router;


//ACTIVITY


router.get('/', async (req, res) => {
	await getTask(req.body.name).then(result => res.send(result))
})


router.get('/tasks/:id', async (req, res) => {
	const id = req.params.taskId
	await getTask(id).then(response => res.send(response))
})

router.put('/tasks/:id/complete', async (req, res) => {
	const id = req.params.taskId
	await updateTask(id, req.body).then(response => res.send(response))
})