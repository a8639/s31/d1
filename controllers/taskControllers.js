const req = require("express/lib/request")
const Task = require(`./../models/Task`)

// Business Logic for Creating a task
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message `Duplicate Task found`
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

//
module.exports.createTask = async (reqBody) => {

	return await Task.findOne({name: reqBody.name}).then( (result, err) => {
		// console.log(result)	//a document

		// If the task already exists in the database, we return a message `Duplicate Task found`
		if(result != null && result.name == reqBody.name){
			return true

		} else {

			if(result == null){

				let newTask = new Task({
					name: reqBody.name
				})

				return newTask.save().then( result => result )

			}
		}
	})

}



module.exports.getAllTasks = async () => {

	return await Task.find().then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})

	//or

	// return await Task.find({}, (docs, err) => {
	// 	if(docs){
	// 		return res.status(200).send(docs)
	// 	} else {
	// 		return res.status(500).json(err)
	// 	}
	// })
}


module.exports.deleteTask = async (id) => {
	
	return await Task.findByIdAndDelete(id).then( result => {
		try {
			if (result != null) {
				return true
			} else {
				return false
			}
		}catch(err) {
			return err
		}
	})
}


module.exports.updateTask = async (id, name, reqBody) => {
	//return await Task.findByIdAndUpdate(id, {$set: reqBody}, {new:true}).then(result => result)


	return await Task.findOneAndUpdate({name: name}, {$set: reqBody}, {new: true}).then(response => {
		if (response == null){
			return {message: `no existing document`}
		} else {
			if (response != null) {
				return response
			}
		}
	})
}


//ACTIVITY

module.exports.getTask = async (reqBody) => {
	return await Task.findOne({name: reqBody.name}).then(result => result)
}

module.exports.getTask = async(id) => {
	return await Task.findById(id).then(result => result)

}

module.exports.updateTask = async(id, name) => {
	return await Task.findByIdAndUpdate (id, {$set: reqBody}, {new: true}).then(result => result)
}